import 'package:calcitems/app/screens/home/home_controller.dart';
import 'package:calcitems/app/shared/models/item_model.dart';
import 'package:flutter/material.dart';

class ItemPage extends StatefulWidget {
  final Item item;
  final HomeController homeController;

  ItemPage({this.item, this.homeController});

  @override
  _ItemPageState createState() => _ItemPageState(homeController);
}

class _ItemPageState extends State<ItemPage> {
  final HomeController controller;
  final nameController = TextEditingController();
  final priceController = TextEditingController();
  FocusNode priceFocusNode;

  Item _editedItem;
  bool novoItem;

  _ItemPageState(this.controller);

  @override
  void initState() {
    super.initState();

    priceFocusNode = FocusNode();
    if (widget.item == null) _editedItem = Item();
  }

  @override
  void dispose() {
    nameController.dispose();
    priceController.dispose();
    priceFocusNode.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Adicionando item'),
        ),
        body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  autofocus: true,
                  onFieldSubmitted: (term) {
                    FocusScope.of(context).requestFocus(priceFocusNode);
                  },
                  validator: (value) {
                    if (value.isEmpty) return "nome é um valor obrigatório";
                  },
                  style: TextStyle(fontSize: 18.0),
                  controller: nameController,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(labelText: "Nome"),
                  onChanged: (text) {
                    setState(() {
                      _editedItem.name = text;
                    });
                  },
                ),
                const SizedBox(height: 10),
                TextFormField(
                  focusNode: priceFocusNode,
                  autofocus: true,
                  validator: (value) {
                    if (value.isEmpty) return "Preço é um valor obrigatório";
                  },
                  style: TextStyle(fontSize: 18.0),
                  controller: priceController,
                  decoration: InputDecoration(
                    labelText: "Preço",
                  ),
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  onChanged: (text) {
                    setState(() {
                      _editedItem.price =
                          double.parse(text.replaceAll(",", "."));
                    });
                  },
                ),
                const SizedBox(height: 20),
                SizedBox(
                  height: 50,
                  width: 250,
                  child: RaisedButton(
                    child: const Text('Adicionar novo item',
                        style: TextStyle(fontSize: 20)),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        controller.add(_editedItem);
                        Navigator.pop(context, true);
                      }
                    },
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  height: 50,
                  width: 250,
                  child: RaisedButton(
                    child: const Text('Salvar / Fechar',
                        style: TextStyle(fontSize: 20)),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        controller.add(_editedItem);
                        Navigator.pop(context, false);
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
