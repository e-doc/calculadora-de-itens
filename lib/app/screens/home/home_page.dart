import 'package:calcitems/app/app_bloc.dart';
import 'package:calcitems/app/app_module.dart';
import 'package:calcitems/app/screens/home/components/item_widget.dart';
import 'package:calcitems/app/screens/home/home_controller.dart';
import 'package:calcitems/app/screens/item/item_page.dart';
import 'package:calcitems/app/shared/models/item_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controller = HomeController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              AppModule.to.bloc<AppBloc>().changeTheme();
            },
            icon: Icon(Icons.brightness_5),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Observer(
            builder: (_) => Container(
              padding: EdgeInsets.all(15),
              color: Colors.yellow,
              child: Text(
                "Total de compras R\$: ${controller.total.toStringAsFixed(2)} ",
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Observer(
            builder: (_) => Expanded(
                child: ListView.builder(
              itemCount: controller.itemsTotal,
              itemBuilder: (context, index) {
                print("montado objeto $index");
                Item item = controller.list[index];
                return ItemWidget(item: item, onTap: showOptions);
              },
            )),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showItemPage,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  _showItemPage({Item item}) async {
    final novoItem = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ItemPage(item: item, homeController: controller)));

    if (novoItem == true) {
      _showItemPage();
    }
  }

  void showOptions(Item item) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return BottomSheet(
            onClosing: () {},
            builder: (context) {
              return Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    // Padding(
                    //   padding: EdgeInsets.all(10.0),
                    //   child: FlatButton(
                    //     child: Text(
                    //       "Editar",
                    //       style: TextStyle(color: Colors.red, fontSize: 20.0),
                    //     ),
                    //     onPressed: () {
                    //       Navigator.pop(context);
                    //       // _showContactPage(contact: contacts[index]);
                    //     },
                    //   ),
                    // ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FlatButton(
                        child: Text(
                          "Excluir",
                          style: TextStyle(color: Colors.red, fontSize: 20.0),
                        ),
                        onPressed: () {
                          controller.remove(item);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        });
  }
}
