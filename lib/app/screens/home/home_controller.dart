import 'package:calcitems/app/shared/models/item_model.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeController with _$HomeController;

abstract class _HomeController with Store {
  @observable
  double total = 0;

  @observable
  ObservableList<Item> list = ObservableList<Item>();

  @action
  add(Item item) {
    list.add(item);
    total = total += item.price;
    print("item adicionado");
  }

  @action
  remove(Item item) {
    list.remove(item);
    total = total -= item.price;
    print("item removido");
  }

  @computed
  int get itemsTotal => list.length;
}
