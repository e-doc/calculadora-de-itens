
class Item {
  String name;
  double price;

  Item();

  @override
  String toString() {
    return "Item(name: $name, price: $price)";
  }
}