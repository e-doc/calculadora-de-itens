import 'package:calcitems/app/app_bloc.dart';
import 'package:calcitems/app/app_module.dart';
import 'package:calcitems/app/screens/home/home_page.dart';
import 'package:calcitems/app/shared/themes/dark_theme.dart';
import 'package:calcitems/app/shared/themes/light_theme.dart';
import 'package:flutter/material.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
     stream: AppModule.to.bloc<AppBloc>().theme,
      initialData: false,
      builder: (context, snapshot) {
        return MaterialApp(
          title: 'Itens de compra',
          theme: snapshot.data ? darkTheme : lightTheme,
          debugShowCheckedModeBanner: false,
          home: MyHomePage(title: "Calculadora de itens"),
        );
      }
    );
  }
}